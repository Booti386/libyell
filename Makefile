MKS_PROJDIR := .
MKS_TOP_BUILDDIR := .
MKS := makesys

# MKS_PROJDIR :=
# MKS_TOP_BUILDDIR :=
# MKS :=

targets := all build clean defconfig test

$(targets):
	+make -C $(MKS_TOP_BUILDDIR) -f $(MKS)/Makefile $(MAKECMDGOALS) MKS_PROJDIR=$(MKS_PROJDIR) MKS_TOP_BUILDDIR=$(MKS_TOP_BUILDDIR)
