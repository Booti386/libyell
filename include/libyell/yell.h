#ifndef LIBYELL_YELL_H
#define LIBYELL_YELL_H 1

struct yell_context;

enum yell_bool
{
	YELL_FALSE = 0u,
	YELL_TRUE = 1u
};
typedef enum yell_bool yell_bool_t;

enum yell_format
{
	YELL_FORMAT_F32LE = 0u,
	YELL_FORMAT_F32BE
};

typedef int (*yell_input_callback_t)(struct yell_context *context, unsigned int frame_idx, unsigned int frame_len, const void **frame_data, void *param);

struct yell_context_vtable
{
	int (*set_resampled)(struct yell_context *context, yell_bool_t resampled);
	int (*set_interleaved)(struct yell_context *context, yell_bool_t interleaved);
	int (*set_format)(struct yell_context *context, enum yell_format format);
	int (*set_channels_count)(struct yell_context *context, unsigned int count);
	int (*set_rate)(struct yell_context *context, unsigned int rate);
	int (*set_input_callback)(struct yell_context *context, yell_input_callback_t callback, void *param);
	int (*start)(struct yell_context *context);
	int (*pause)(struct yell_context *context);
	int (*resume)(struct yell_context *context);
	int (*stop)(struct yell_context *context);
	int (*destroy)(struct yell_context *context);
};

struct yell_context
{
	const struct yell_context_vtable *vtbl;
};

#endif /* LIBYELL_YELL_H */
