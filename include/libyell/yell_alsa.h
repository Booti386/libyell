#ifndef LIBYELL_YELL_ALSA_H
#define LIBYELL_YELL_ALSA_H 1

extern struct yell_context *yell_alsa_init(const char *pcm_name);

#endif /* LIBYELL_YELL_ALSA_H */
