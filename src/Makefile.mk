SUBDIRS-y :=
EXTDIRS-y :=

libyell := libyell$(DYN_LIB_EXT)

BINS-y := $(libyell)

objs := \
	yell_alsa.c.o

CFLAGS-y += -fPIC -fvisibility=hidden -I$(MKS_PROJDIR)/include

CFLAGS-yell_alsa.c.o-y += $(LIBASOUND_CFLAGS)

LDFLAGS-$(libyell)-y += $(LDFLAGS_SHARED) $(LIBASOUND_CFLAGS) $(LIBASOUND_LDFLAGS)

OBJS-$(libyell)-y := $(objs)
