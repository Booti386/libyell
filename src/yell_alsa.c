/* For alsa/asounlib.h */
#define _POSIX_C_SOURCE 1

#include <stdlib.h>
#include <poll.h>
#include <pthread.h>

#include <alsa/asoundlib.h>

#include "libyell/yell.h"
#include "libyell/yell_alsa.h"
#include "yell_priv.h"
#include "yell_alsa_priv.h"

struct yell_context_alsa
{
	struct yell_context base;
	snd_pcm_t *pcm_handle;

	yell_bool_t hw_params_updated : 1;
	yell_bool_t hw_resampled : 1;
	yell_bool_t hw_interleaved : 1;
	enum yell_format hw_format;
	unsigned int hw_channels_count;
	unsigned int hw_rate;
	unsigned int hw_buffer_time;
	unsigned int hw_period_time;
	snd_pcm_uframes_t hw_buffer_size;
	snd_pcm_uframes_t hw_period_size;

	yell_bool_t sw_params_updated : 1;

	yell_input_callback_t input_callback;
	void *input_callback_param;

	unsigned int cur_frame_idx;

	pthread_t poll_thread;
	yell_bool_t poll_thread_alive : 1;
};


static inline struct yell_context_alsa *impl_from_yell_context(struct yell_context *context) {
	return CONTAINER_OF(context, struct yell_context_alsa, base);
}

static int yell_context_alsa_set_resampled(struct yell_context *context, yell_bool_t resampled)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->hw_resampled = resampled;
	ctx->hw_params_updated = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_set_interleaved(struct yell_context *context, yell_bool_t interleaved)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->hw_interleaved = interleaved;
	ctx->hw_params_updated = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_set_format(struct yell_context *context, enum yell_format format)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->hw_format = format;
	ctx->hw_params_updated = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_set_channels_count(struct yell_context *context, unsigned int count)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->hw_channels_count = count;
	ctx->hw_params_updated = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_set_rate(struct yell_context *context, unsigned int rate)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->hw_rate = rate;
	ctx->hw_params_updated = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_set_input_callback(struct yell_context *context, yell_input_callback_t callback, void *param)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	ctx->input_callback = callback;
	ctx->input_callback_param = param;

	return 0;
}

static void *yell_alsa_poll_thread_func(void *param)
{
	struct yell_context_alsa *ctx = (struct yell_context_alsa *)param;
	int ret;
	int fds_count;
	struct pollfd *fds = NULL;

	fds_count = snd_pcm_poll_descriptors_count(ctx->pcm_handle);
	if (fds_count <= 0)
		goto free_exit;

	fds = malloc(fds_count * sizeof(fds[0]));
	if (!fds)
		goto free_exit;

	ret = snd_pcm_poll_descriptors(ctx->pcm_handle, fds, fds_count);
	if (ret < 0)
		goto free_exit;

	ctx->cur_frame_idx = 0;

	while (1)
	{
		unsigned short revents;
		const void *frame_data = NULL;

		if (ctx->cur_frame_idx != 0)
		{
			poll(fds, fds_count, -1);

			snd_pcm_poll_descriptors_revents(ctx->pcm_handle, fds, fds_count, &revents);
			if (!(revents & POLLOUT))
				continue;
		}

		ret = ctx->input_callback(&ctx->base, ctx->cur_frame_idx, ctx->hw_period_size * ctx->hw_channels_count, &frame_data, ctx->input_callback_param);
		if (ret < 0 || !frame_data)
			break;

		ret = snd_pcm_writei(ctx->pcm_handle, frame_data, ctx->hw_period_size);
		if (ret < 0)
			ret = snd_pcm_recover(ctx->pcm_handle, ret, 1);
		if (ret < 0)
			break;

		ctx->cur_frame_idx += ctx->hw_period_size * ctx->hw_channels_count;
	}

free_exit:
	if (fds)
		free(fds);

	return NULL;
}

static int yell_context_alsa_start(struct yell_context *context)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);
	int ret;

	if (!ctx->input_callback)
		return -1;

	if (ctx->poll_thread_alive)
		return -1;

	if (ctx->hw_params_updated)
	{
		snd_pcm_hw_params_t *params = NULL;
		snd_pcm_access_t access = ctx->hw_interleaved ? SND_PCM_ACCESS_RW_INTERLEAVED  : SND_PCM_ACCESS_RW_NONINTERLEAVED;
		snd_pcm_format_t format = alsa_format_from_yell_format(ctx->hw_format);
		unsigned int rate = ctx->hw_rate;
		unsigned int buffer_time = ctx->hw_buffer_time;
		snd_pcm_uframes_t buffer_size = 0;
		unsigned int period_time = ctx->hw_period_time;
		snd_pcm_uframes_t period_size = 0;

		ret = snd_pcm_hw_params_malloc(&params);
		if (ret < 0 || !params)
			return -1;

		ret = snd_pcm_hw_params_any(ctx->pcm_handle, params);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_set_rate_resample(ctx->pcm_handle, params, ctx->hw_resampled);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_set_access(ctx->pcm_handle, params, access);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_set_format(ctx->pcm_handle, params, format);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_set_channels(ctx->pcm_handle, params, ctx->hw_channels_count);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_set_rate_near(ctx->pcm_handle, params, &rate, NULL);
		if (ret < 0)
			return -1;
		if (rate != ctx->hw_rate)
			return -1;

		ret = snd_pcm_hw_params_set_buffer_time_near(ctx->pcm_handle, params, &buffer_time, NULL);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_get_buffer_size(params, &buffer_size);
		if (ret < 0)
			return -1;
		ctx->hw_buffer_size = buffer_size;

		ret = snd_pcm_hw_params_set_period_time_near(ctx->pcm_handle, params, &period_time, NULL);
		if (ret < 0)
			return -1;

		ret = snd_pcm_hw_params_get_period_size(params, &period_size, NULL);
		if (ret < 0)
			return -1;
		ctx->hw_period_size = period_size;

		ret = snd_pcm_hw_params(ctx->pcm_handle, params);
		if (ret < 0)
			return -1;

		free(params);

		ctx->hw_params_updated = YELL_FALSE;
	}

	if (ctx->sw_params_updated)
	{
		snd_pcm_sw_params_t *params = NULL;

		ret = snd_pcm_sw_params_malloc(&params);
		if (ret < 0 || !params)
			return -1;

		ret = snd_pcm_sw_params_current(ctx->pcm_handle, params);
		if (ret < 0)
			return -1;

		ret = snd_pcm_sw_params_set_start_threshold(ctx->pcm_handle, params, (ctx->hw_buffer_size / ctx->hw_period_size) * ctx->hw_period_size);
		if (ret < 0)
			return -1;

		ret = snd_pcm_sw_params_set_avail_min(ctx->pcm_handle, params, ctx->hw_period_size);
		if (ret < 0)
			return -1;

		ret = snd_pcm_sw_params(ctx->pcm_handle, params);
		if (ret < 0)
			return -1;

		free(params);

		ctx->sw_params_updated = YELL_FALSE;
	}

	ret = snd_pcm_prepare(ctx->pcm_handle);
	if (ret < 0)
		return -1;

	ret = snd_pcm_start(ctx->pcm_handle);
	if (ret < 0)
		return -1;

	pthread_create(&ctx->poll_thread, NULL, yell_alsa_poll_thread_func, (void *)ctx);

	ctx->poll_thread_alive = YELL_TRUE;

	return 0;
}

static int yell_context_alsa_pause(struct yell_context *context)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);
	int ret;

	ret = snd_pcm_pause(ctx->pcm_handle, 1);
	if (ret < 0)
		return -1;

	return 0;
}

static int yell_context_alsa_resume(struct yell_context *context)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);
	int ret;

	ret = snd_pcm_pause(ctx->pcm_handle, 0);
	if (ret < 0)
		return -1;

	return 0;
}

static int yell_context_alsa_stop(struct yell_context *context)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);
	int ret;

	ret = snd_pcm_drop(ctx->pcm_handle);
	if (ret < 0)
		return -1;

	pthread_join(ctx->poll_thread, NULL);

	ctx->poll_thread_alive = YELL_FALSE;

	return 0;
}

static int yell_context_alsa_destroy(struct yell_context *context)
{
	struct yell_context_alsa *ctx = impl_from_yell_context(context);

	snd_pcm_close(ctx->pcm_handle);
	free(ctx);

	return 0;	
}

static const struct yell_context_vtable yell_alsa_vtbl = {
	yell_context_alsa_set_resampled,
	yell_context_alsa_set_interleaved,
	yell_context_alsa_set_format,
	yell_context_alsa_set_channels_count,
	yell_context_alsa_set_rate,
	yell_context_alsa_set_input_callback,
	yell_context_alsa_start,
	yell_context_alsa_pause,
	yell_context_alsa_resume,
	yell_context_alsa_stop,
	yell_context_alsa_destroy
};

EXPORT struct yell_context *yell_alsa_init(const char *pcm_name)
{
	struct yell_context_alsa *ctx;
	int ret;

	if (!pcm_name)
		pcm_name = "default";

	ctx = malloc(sizeof(*ctx));
	if (!ctx)
		return NULL;

	ctx->base.vtbl = &yell_alsa_vtbl;

	ctx->hw_params_updated = YELL_TRUE;
	ctx->hw_resampled = YELL_TRUE;
	ctx->hw_interleaved = YELL_TRUE;
	ctx->hw_format = YELL_FORMAT_F32LE;
	ctx->hw_channels_count = 2;
	ctx->hw_rate = 44100;
	ctx->hw_buffer_time = 500000;
	ctx->hw_period_time = 100000;
	ctx->hw_buffer_size = 0;
	ctx->hw_period_size = 0;

	ctx->sw_params_updated = YELL_TRUE;

	ctx->input_callback = (yell_input_callback_t)NULL;
	ctx->input_callback_param = NULL;

	ctx->cur_frame_idx = 0;

	ctx->poll_thread_alive = YELL_FALSE;

	ret = snd_pcm_open(&ctx->pcm_handle, pcm_name, SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK);
	if (ret < 0)
		return NULL;

	return &ctx->base;
}
