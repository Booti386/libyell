#ifndef YELL_ALSA_PRIV_H
#define YELL_ALSA_PRIV_H 1

static inline snd_pcm_format_t alsa_format_from_yell_format(enum yell_format format) {
	switch (format)
	{
		case YELL_FORMAT_F32LE:
			return SND_PCM_FORMAT_FLOAT_LE;

		case YELL_FORMAT_F32BE:
			return SND_PCM_FORMAT_FLOAT_BE;

		default:
			break;
	}

	return SND_PCM_FORMAT_UNKNOWN;
}

#endif /* YELL_ALSA_PRIV_H */
