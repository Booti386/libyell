#ifndef YELL_PRIV_H
#define YELL_PRIV_H 1

#include <stddef.h> /* offsetof() */
#include <stdint.h>

#define EXPORT __attribute__((visibility("default")))

#define CONTAINER_OF(ptr, type, member) ((type *)((uintptr_t)(ptr) - offsetof(type, member)))

#endif /* YELL_PRIV_H */
